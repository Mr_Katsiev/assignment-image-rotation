#include "image.h"
#include "bmp.h"
#include "rotate.h"
#include <stdio.h>

#define err(ST) fprintf(stderr, ST)

int main( int argc, char** argv ) {
	if (argc != 3)
	{
		err("Required 2 arguments!\n");
		return 1;
	}

	FILE *in = fopen(argv[1], "rb");
	if (in == NULL)
	{
		err("Failed to open inputted file!\n");
		return 1;
	}
	
	struct image img = { 0 };
	enum read_status status = from_bmp(in, &img);
	if (status != READ_OK) {
		if (status == READ_INVALID_SIGNATURE)
			err("INVALID SIGNATURE of inputted file!\n");
		else if (status == READ_INVALID_BITS)
			err("INVALID BITS of inputted file!\n");
		else if (status == READ_INVALID_HEADER)
			err("INVALID HEADER of inputted file!\n");
		return 1;
	}

	if (fclose(in))
	{
		err("Failed to close inputted file!\n");
		return 1;
	}

	struct image res = rotate(img);

	FILE *out = fopen(argv[2], "wb");
	if (out == NULL) {
		err("Failed to open resulted image!\n");
		return 1;
	}

	if (to_bmp(out, &res) != WRITE_OK)
	{
		err("Failed to write resulted image!\n");
		return 1;
	}

	if (fclose(out))
	{
		err("Failed to close resulted image!\n");
		return 1;
	}

	image_destroy(&img);
	image_destroy(&res);
	
	return 0;
}
