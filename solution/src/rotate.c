#include "rotate.h"

struct image rotate(struct image const img) {
	struct image rotated = image_create(img.height, img.width);
	for (size_t height = 0; height < rotated.height; height++)
		for (size_t width = 0; width < rotated.width; width++)
			rotated.data[rotated.width * height + width] = img.data[img.width * (img.height - width - 1) + height];
			//rotated.data[img.height * width + (img.height - height - 1)] = img.data[img.width * height + width];
	return rotated;
}
