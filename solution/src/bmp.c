#include "bmp.h"
#include <stdlib.h>

#define BMP_TYPE 19778
#define HEADER_SZ 40
#define BITS_PIXEL 24
#define BPLANES 1


size_t calc_padding(size_t width) {
	return 4 - (width * sizeof(struct pixel) % 4) % 4;
}

size_t calc_size(const struct image* img) {
	return (img->width * sizeof(struct pixel) + calc_padding(img->width)) * img->height;
}

struct bmp_header get_header(const struct image* img) {
	return (struct bmp_header) {
			.bfType = BMP_TYPE,
			.bfileSize = sizeof(struct bmp_header) + calc_size(img),
			.bfReserved = 0,
			.bOffBits = 0,
			.biSize = HEADER_SZ,
			.biWidth = img->width,
			.biHeight = img->height,
			.biPlanes = BPLANES,
			.biBitCount = BITS_PIXEL,
			.biCompression = 0,
			.biSizeImage = calc_size(img),
			.biXPelsPerMeter = 0,
			.biYPelsPerMeter = 0,
			.biClrUsed = 0,
			.biClrImportant = 0
	};
}

enum read_status from_bmp(FILE* in, struct image* img) {
	struct bmp_header header = { 0 };
	if (!fread(&header, sizeof(struct bmp_header), 1, in))
		return READ_INVALID_HEADER;

	*img = image_create(header.biWidth, header.biHeight);
	uint8_t padd = calc_padding((*img).width);

	for (size_t i = 0; i < (*img).height; i++) {
		size_t count = fread((*img).data + (*img).width * i, sizeof(struct pixel), (*img).width, in);
		if (count != (*img).width)
			return READ_INVALID_BITS;
		if (fseek(in, padd, SEEK_CUR) != 0)
			return READ_INVALID_SIGNATURE;
	}
	return READ_OK;
}


enum write_status to_bmp(FILE* out, struct image const* img) {
	struct bmp_header header = get_header(img);
	if (!fwrite(&header, sizeof(struct bmp_header), 1, out))
		return WRITE_ERROR;

	uint8_t padd = calc_padding((*img).width);
	uint8_t padding_value = 0;

	for (size_t i = 0; i < img->height; i++) {
		if (!fwrite((*img).data + i * (*img).width, sizeof(struct pixel), (*img).width, out))
			return WRITE_ERROR;
		if (!fwrite(&padding_value, sizeof(padding_value), padd, out))
			return WRITE_ERROR;
	}

	return WRITE_OK;
}

